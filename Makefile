#
# Makefile
#

CFLAGS=-Wall -Wextra -O1

# -- targets --

all: xor

clean:
	rm -f xor *.o *.ppm

xor: xor.o perceptron.o image.o util.o
	$(CC) $(CFLAGS) $^ -o $@ -lm

# -- dependencies --

xor.o: xor.c perceptron.h image.h util.h

perceptron.o: perceptron.c perceptron.h util.h

image.o: image.c image.h util.h

util.g.o: random.c random.h

.PHONY: all clean

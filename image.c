// Copyright 2022 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

#include "image.h"

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>


static int IM_ParseInt(FILE *fp)
{
	// skip whitespace, including comments
	for (;;)
	{
		int ch = fgetc(fp);
		if (ch == EOF)
			return -1;

		if ((ch & 255) <= ' ')
			continue;

		if (ch != '#')
		{
			ungetc(ch, fp);
			break;
		}

		// comments extend to end of the line
		for (;;)
		{
			ch = fgetc(fp);
			if (ch == EOF)
				return -1;

			if (ch == '\n')
				break;
		}
	}

	int result = 0;

	if (fscanf(fp, "%d", &result) != 1)
		return -1;

	return result;
}


int IM_Load(struct Image *img, const char *filename)
{
	assert(img);

	FILE *fp = fopen(filename, "rb");
	if (fp == NULL)
	{
		return -1;
	}

	// check the header...

	char header[4];

	if (fread(header, 2, 1, fp) != 1)
	{
		fclose(fp);
		return -1;
	}

	if (header[0] != 'P' || header[1] != '6')
	{
		fprintf(stderr, "file is not PPM format: %s\n", filename);
		fclose(fp);
		return -1;
	}

	int width  = IM_ParseInt(fp);
	int height = IM_ParseInt(fp);
	int maxval = IM_ParseInt(fp);

	// check the size info...

	if (width <= 0 || height <= 0 || maxval < 0)
	{
		fprintf(stderr, "error parsing PPM file: %s\n", filename);
		fclose(fp);
		return -1;
	}

	if (width > MAX_IMAGE || height > MAX_IMAGE)
	{
		fprintf(stderr, "PPM file is too big: %s\n", filename);
		fclose(fp);
		return -1;
	}

	if (maxval > 255)
	{
		fprintf(stderr, "16-bit samples in PPM file: %s\n", filename);
		fclose(fp);
		return -1;
	}

	img->w = width;
	img->h = height;

	// skip the single char of whitespace
	(void) fgetc(fp);

	// read the pixels
	int x, y;

	for (y = 0 ; y < height ; y++)
	{
		for (x = 0 ; x < width ; x++)
		{
			int r = fgetc(fp);
			int g = fgetc(fp);
			int b = fgetc(fp);

			if (r == EOF || g == EOF || b == EOF)
			{
				fprintf(stderr, "error reading PPM data in: %s\n", filename);
				fclose(fp);
				return -1;
			}

			img->pix[x][y] = RGB(r, g, b);
		}
	}

	return 0;  // ok
}


int IM_Save(struct Image *img, const char *filename)
{
	assert(img);

	FILE *fp = fopen(filename, "wb");
	if (fp == NULL)
	{
		return -1;
	}

	fprintf(fp, "P6 %d %d %d\n", img->w, img->h, 255);

	int x, y;

	for (y = 0 ; y < img->h ; y++)
	{
		for (x = 0 ; x < img->w ; x++)
		{
			int pix = img->pix[x][y];

			int r = RED(pix);
			int g = GREEN(pix);
			int b = BLUE(pix);

			fputc(r, fp);
			fputc(g, fp);
			fputc(b, fp);
		}
	}

	fclose(fp);

	return 0;  // ok
}


void IM_Invert(struct Image *img)
{
	int x, y;

	for (x = 0 ; x < img->w ; x++)
	{
		for (y = 0 ; y < img->h / 2; y++)
		{
			int z = img->h - y - 1;

			Pixel p1 = img->pix[x][y];
			Pixel p2 = img->pix[x][z];

			img->pix[x][y] = p2;
			img->pix[x][z] = p1;
		}
	}
}

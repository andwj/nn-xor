// Copyright 2022 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

#define MAX_IMAGE  1024

typedef unsigned int Pixel;

#define RGB(r, g, b)  ((Pixel) (((r) << 16) | ((g) << 8) | (b)))

#define RED(pix)    ((int)((pix) >> 16) & 255)
#define GREEN(pix)  ((int)((pix) >>  8) & 255)
#define BLUE(pix)   ((int) (pix)        & 255)

struct Image
{
	int w;
	int h;

	// pixels are accessed like this: img->pix[x][y]
	// where x == 0 is at the left, y == 0 is at the top.
	Pixel pix[MAX_IMAGE][MAX_IMAGE];
};

int IM_Load(struct Image *img, const char *filename);
int IM_Save(struct Image *img, const char *filename);

void IM_Invert(struct Image *img);


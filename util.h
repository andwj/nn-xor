// Copyright 2022 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

/* Random Numbers */

void   Rand_Seed(int seed);
int    Rand_Range(int low, int high);
double Rand_Float(void);

/* Strings */

int MatchNoCase (const char *A, const char *B);
int PartialMatch(const char *s, const char *prefix);
int PartialMatchNoCase(const char *s, const char *prefix);

/* Geometry */

double CalcAngle(double dx, double dy);
double AngleDiff(double a, double b);
double PerpDist(double px, double py, double x1, double y1, double x2, double y2);



NN-Xor
======

by Andrew Apted, 2022.


About
-----

This repository implements a *parallel* perceptron and trains it to produce
the XOR operation.  It is well know that a standard (single layer) perceptron
is unable to produce an XOR of its inputs.  The simplicity of this task makes
it a good one for exploring the implementation of a parallel perceptron.

Parallel perceptrons are described in a paper called "A learning rule for very
simple universal approximators consisting of a single layer of perceptrons" by
Peter Auer, Harald Burgsteiner and Wolfgang Maass.

The basic idea is to have N single layer perceptrons all running in parallel
on the inputs, and to combine ("squash") their results into a final output
value.  For a boolean output, the number N is usually odd, and the squash
function just selects the majority value.

For training, each perceptron begins with random weights (this is an assumption
on my part, the paper does not mention this).  For each training sample, a rule
called the "pdelta" rule applies to each perceptron to update its weights.
Only perceptrons which give the wrong result need to be updated.  Eventually
the system should converge to something approximating the function (the mapping
of inputs to outputs) as well as possible.


Compiling
---------

You will need a C99 compiler and a make program.  Just typing `make` on a
Unix-like system is enough to build the program.  The Makefile is basic and
should work with BSD make (though I have only tested with GNU make).


Running
-------

This is a command-line program, which needs to be run from a terminal.

Running `xor` with no options will train a single "brain" (a set of perceptrons)
using a default random seed, and a default number of steps, and show the output
of the final brain.

Run it with an unknown option, such as `-h`, to show a list of supported options.
Note that options must be separated by their value by whitespace.

The `-s` option specifies the random seed.

The `-t` option specifies the number of training steps.

The `-r` and `-m` options give the learning rate and margin.  The default rate
is 0.05 and the default margin is 0.25.  Higher rates may learn more quickly, but
are more likely to give a poor result.  Margins higher than 0.5 seems to require
an order of magnitude (or two!) of training steps on certain seeds to get a
working classifier.

The `-c` option will run the training process on multiple random seeds (beginning
with the seed specified with the `-s` option).  What is shown on the terminal is
also changed, only showing whether each seed built a working classifier or not.


Example
-------

The following image is generated when `xor` is run with default parameters.
The range of the X and Y axes is from -2.0 to +2.0, where black represents an
output of -1.0 from the brain, gray represents an output of +1.0, and the white
dots show the coordinates of the inputs used to train the XOR function.

![example](example.png)


Legalese
--------

I wrote this software from scratch.

This software is under a permissive MIT license.

This software comes with NO WARRANTY of any kind, express or implied.

See the [LICENSE.md](LICENSE.md) document for the full terms.


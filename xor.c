// Copyright 2022 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

#include "perceptron.h"
#include "image.h"
#include "util.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


int SEED  = 1;
int DETER = 0;
int COUNT = 1;
int STEPS = 20;
int SHOW_STEPS = 0;

double RATE   = 0.05;
double MARGIN = 0.25;


// these define the boolean function to map, currently XOR.
// the values must be either -1.0 or +1.0.
#define OUTPUT_00  -1.0
#define OUTPUT_01  +1.0
#define OUTPUT_10  +1.0
#define OUTPUT_11  -1.0


struct Brain brain;

struct Image image;


int TrainPair(double a, double b, double output)
{
	struct Perceptron inputs;

	inputs.w[0] = a;
	inputs.w[1] = b;
	inputs.w[2] = +1.0;

	return P_TrainBrain(&brain, RATE, MARGIN, &inputs, output);
}


void Train(int step)
{
	(void) step;

	int t1 = TrainPair(-1.0, -1.0, OUTPUT_00);
	int t2 = TrainPair(-1.0, +1.0, OUTPUT_01);
	int t3 = TrainPair(+1.0, -1.0, OUTPUT_10);
	int t4 = TrainPair(+1.0, +1.0, OUTPUT_11);

	if (COUNT == 1)
	{
		printf("step [%04d] : %d %d %d %d\n", step, t1, t2, t3, t4);
	}
}


double TestPair(double a, double b, double output)
{
	struct Perceptron inputs;

	inputs.w[0] = a;
	inputs.w[1] = b;
	inputs.w[2] = +1.0;

	P_ComputeBrain(&brain, &inputs);

	if (COUNT == 1)
	{
		printf("test pair: %+1.3f xor %+1.3f = %+1.3f\n", a, b, brain.output);
	}

	return (fabs(brain.output - output) < 0.1);
}


int TestBrain(int no_message)
{
	if (COUNT == 1 && !no_message)
	{
		printf("\n");
	}

	int b1 = TestPair(-1.0, -1.0, OUTPUT_00);
	int b2 = TestPair(-1.0, +1.0, OUTPUT_01);
	int b3 = TestPair(+1.0, -1.0, OUTPUT_10);
	int b4 = TestPair(+1.0, +1.0, OUTPUT_11);

	int total = b1 + b2 + b3 + b4;

	if (COUNT > 1 && !no_message)
	{
		printf("seed %04d : %s\n", DETER ? 0 : SEED, (total == 4) ? "OK" : "FAILED");
		fflush(stdout);
	}

	return (total == 4);
}


void CreateImage(void)
{
	image.w = 512;
	image.h = 512;

	int x, y;

	for (y = 0 ; y < 512 ; y++)
	{
		// wanted range is -2.0 to +2.0
		double b = ((double)y / 512.0 - 0.5) * 4.0;

		// invert image (so that -2.0 is at bottom)
		b = -b;

		for (x = 0 ; x < 512 ; x++)
		{
			double a = ((double)x / 512.0 - 0.5) * 4.0;

			struct Perceptron inputs;

			inputs.w[0] = a;
			inputs.w[1] = b;
			inputs.w[2] = +1.0;

			P_ComputeBrain(&brain, &inputs);

			if ((x == 127 || x == 128 || x == 383 || x == 384) &&
			    (y == 127 || y == 128 || y == 383 || y == 384))
			{
				// place dots at the binary input coords (+/-1, +/-1)
				image.pix[x][y] = RGB(255, 255, 255);
			}
			else if (x == 256 || y == 256)
			{
				// draw the X and Y axes
				image.pix[x][y] = RGB(255, 0, 0);
			}
			else if (x == 0 || x == 511 || y == 0 || y == 511)
			{
				// draw a border
				image.pix[x][y] = RGB(0, 0, 255);
			}
			else
			{
				if (brain.output < 0)
					image.pix[x][y] = RGB(0, 0, 0);
				else
					image.pix[x][y] = RGB(64, 64, 64);
			}
		}
	}

	char filename[256];

	snprintf(filename, sizeof(filename), "IMAGE-%04d.ppm", DETER ? 0 : SEED);

	IM_Save(&image, filename);
}

//----------------------------------------------------------------------

void ShowUsage(void)
{
	printf("SUPPORTED OPTIONS:\n");
	printf("   -c  <count>\n");
	printf("   -s  <seed>\n");
	printf("   -t  <steps>\n");
	printf("   -r  <rate>\n");
	printf("   -m  <margin>\n");
	fflush(stdout);
}


void ParseArgs(int argc, char **argv)
{
	// skip program name
	argv += 1;
	argc -= 1;

	while (argc > 0)
	{
		if (strcmp(argv[0], "-s") == 0 && argc > 1)
		{
			SEED = atoi(argv[1]);

			argv += 2;
			argc -= 2;
			continue;
		}

		if (strcmp(argv[0], "-c") == 0 && argc > 1)
		{
			COUNT = atoi(argv[1]);

			argv += 2;
			argc -= 2;
			continue;
		}

		if (strcmp(argv[0], "-t") == 0 && argc > 1)
		{
			STEPS = atoi(argv[1]);

			argv += 2;
			argc -= 2;
			continue;
		}

		if (strcmp(argv[0], "-r") == 0 && argc > 1)
		{
			RATE = atof(argv[1]);

			argv += 2;
			argc -= 2;
			continue;
		}

		if (strcmp(argv[0], "-m") == 0 && argc > 1)
		{
			MARGIN = atof(argv[1]);

			argv += 2;
			argc -= 2;
			continue;
		}

		ShowUsage();
		exit(1);
	}
}


void DoEverything(int deterministic)
{
	DETER = deterministic;

	if (DETER)
	{
		P_DeterministicInitBrain(&brain);
	}
	else
	{
		Rand_Seed(SEED);

		P_RandomizeBrain(&brain);
	}

	int step;
	for (step = 1 ; step <= STEPS ; step++)
	{
		Train(step);

		if (SHOW_STEPS)
		{
			if (TestBrain(1))
				break;
		}
	}

	if (SHOW_STEPS)
	{
		printf("seed %04d : %d\n", DETER ? 0 : SEED, step);
		fflush(stdout);
	}
	else
	{
		TestBrain(0);

		CreateImage();
	}
}


int main(int argc, char *argv[])
{
	ParseArgs(argc, argv);

//	DoEverything(1);

	int c;
	for (c = 1 ; c <= COUNT ; c++)
	{
		DoEverything(0);

//		P_DumpBrain(&brain);

		SEED += 1;
	}

	return 0;
}

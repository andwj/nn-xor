// Copyright 2022 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

// the Perceptron struct contains the weights of a single perceptron.
// there is an extra weight at the end for the bias.  this structure can
// also be used to hold the inputs for the set of perceptrons.

#define NUM_INPUTS  2

struct Perceptron
{
	double  w[NUM_INPUTS + 1];

	// the raw result of applying this perceptrons to a set of inputs
	double  raw;

	// the rectified value, either -1.0 or +1.0
	double  output;
};


// the Brain struct contains a group of the perceptrons, plus some other
// useful state.  the  output is the majority of the outputs of all the
// perceptrons, either -1.0 or +1.0 (never anything else).

#define NUM_PERCEPTRONS  3

struct Brain
{
	struct Perceptron  p[NUM_PERCEPTRONS];

	// the majority result
	double  output;
};


void P_RandomizeBrain(struct Brain * br);

void P_DeterministicInitBrain(struct Brain * br);

void P_ComputeBrain(struct Brain * br, const struct Perceptron * inputs);

int  P_TrainBrain(struct Brain * br, double rate, double margin, const struct Perceptron * inputs, double output);

void P_DumpBrain(const struct Brain * br);

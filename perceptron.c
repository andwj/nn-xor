// Copyright 2022 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

#include "perceptron.h"
#include "util.h"

#include <math.h>
#include <stdio.h>


#define MARGIN_FACTOR  1.0

// since output of the brain is a boolean (-1.0 or +1.0), the accuracy
// value has almost no effect here.
#define ACCURACY  0.5


double P_CalcLength(struct Perceptron *p)
{
	double len = 0.0;

	int i;
	for (i = 0 ; i < NUM_INPUTS + 1 ; i++)
	{
		len += p->w[i] * p->w[i];
	}

	return sqrt(len);
}


void P_NormalizeUnit(struct Perceptron *p)
{
	double len = P_CalcLength(p);

	if (len > 0.000001)
	{
		double invert = 1.0 / len;

		int i;
		for (i = 0 ; i < NUM_INPUTS + 1 ; i++)
		{
			p->w[i] *= invert;
		}
	}
}


void P_RandomizeUnit(struct Perceptron *p)
{
	int i;
	for (i = 0 ; i < NUM_INPUTS + 1 ; i++)
	{
		p->w[i] = Rand_Float() - 0.5;
	}

	P_NormalizeUnit(p);
}


void P_RandomizeBrain(struct Brain * br)
{
	int i;
	for (i = 0 ; i < NUM_PERCEPTRONS ; i++)
	{
		P_RandomizeUnit(&br->p[i]);
	}
}


void P_DeterministicInitUnit(struct Perceptron *p, int num)
{
	int i;
	for (i = 0 ; i < NUM_INPUTS + 1 ; i++)
	{
		if (num & (1 << i))
			p->w[i] = (i & 1) ? +1.0 : -1.0;
		else
			p->w[i] = 0.0;
	}

	P_NormalizeUnit(p);
}


void P_DeterministicInitBrain(struct Brain * br)
{
	int i;
	for (i = 0 ; i < NUM_PERCEPTRONS ; i++)
	{
		P_DeterministicInitUnit(&br->p[i], i);
	}
}


void P_ComputeUnit(struct Perceptron * p, const struct Perceptron * inputs)
{
	double raw = 0.0;

	int i;
	for (i = 0 ; i < NUM_INPUTS + 1 ; i++)
	{
		raw += p->w[i] * inputs->w[i];
	}

	p->raw = raw;

	if (raw > 0)
		p->output = +1.0;
	else
		p->output = -1.0;
}


void P_ComputeBrain(struct Brain * br, const struct Perceptron * inputs)
{
	int count = 0;

	int i;
	for (i = 0 ; i < NUM_PERCEPTRONS ; i++)
	{
		P_ComputeUnit(&br->p[i], inputs);

		if (br->p[i].output > 0)
			count += 1;
	}

	if (count >= (NUM_PERCEPTRONS + 1) / 2)
		br->output = +1.0;
	else
		br->output = -1.0;
}

//----------------------------------------------------------------------

void P_AddWeights(struct Perceptron *p, double mul, const struct Perceptron *inputs)
{
	int i;
	for (i = 0 ; i < NUM_INPUTS + 1 ; i++)
	{
		p->w[i] += mul * inputs->w[i];
	}
}


void P_TrainUnit(struct Perceptron * p, double rate, double margin,
                 const struct Perceptron * inputs, double diff)
{
	if (diff > ACCURACY && p->raw >= 0)
	{
		// subtract the inputs from the weights
		P_AddWeights(p, - rate, inputs);
		P_NormalizeUnit(p);
	}
	else if (diff < -ACCURACY && p->raw < 0)
	{
		// add the inputs to the weights
		P_AddWeights(p, rate, inputs);
		P_NormalizeUnit(p);
	}
	else if (diff < ACCURACY && (0 < p->raw && p->raw < margin))
	{
		// update for margin
		P_AddWeights(p, rate * MARGIN_FACTOR, inputs);
		P_NormalizeUnit(p);
	}
	else if (diff > -ACCURACY && (-margin < p->raw && p->raw <= 0))
	{
		// update for margin
		P_AddWeights(p, - rate * MARGIN_FACTOR, inputs);
		P_NormalizeUnit(p);
	}
}


int P_TrainBrain(struct Brain * br, double rate, double margin,
                 const struct Perceptron * inputs, double output)
{
	P_ComputeBrain(br, inputs);

	double diff = br->output - output;

	int i;
	for (i = 0 ; i < NUM_PERCEPTRONS ; i++)
	{
		P_TrainUnit(&br->p[i], rate, margin, inputs, diff);
	}

	// the brain is producing the correct output for these inputs?
	if (fabs(diff) < ACCURACY)
		return 1;

	return 0;
}

//----------------------------------------------------------------------

void P_DumpUnit(const struct Perceptron * p)
{
	printf("  |");

	int i;
	for (i = 0 ; i < NUM_INPUTS + 1 ; i++)
	{
		printf("  %+1.6f", p->w[i]);
	}

	printf("\n");
}


void P_DumpBrain(const struct Brain * br)
{
	printf("brain:\n");

	int i;
	for (i = 0 ; i < NUM_PERCEPTRONS ; i++)
	{
		P_DumpUnit(&br->p[i]);
	}

	fflush(stdout);
}

// Copyright 2022 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

#include "util.h"

#include <stdint.h>
#include <ctype.h>
#include <math.h>
#include <assert.h>

// This random number generator is 'xorshift*' by George Marsaglia.
// The multiplication makes it slower than other generators, but I
// prefer the small amount of state and simpler code.  Like other
// xorshift algorithms, the initial state must never be zero.

static uint64_t random_state = 1;

static uint64_t xorshift64s(void)
{
	uint64_t x = random_state;
	x ^= x >> 12;
	x ^= x << 25;
	x ^= x >> 27;
	random_state = x;
	return x * UINT64_C(0x2545F4914F6CDD1D);
}


void Rand_Seed(int seed)
{
	if (seed == 0)
		seed = 1;

	random_state = (uint64_t)seed + UINT64_C(0x123400000000);

	// warm the generator up a bit
	xorshift64s();  xorshift64s();  xorshift64s();
	xorshift64s();  xorshift64s();  xorshift64s();
	xorshift64s();  xorshift64s();  xorshift64s();
}


int Rand_Range(int low, int high)
{
	high -= low;
	if (high <= 0)
		return low;

	uint64_t u = xorshift64s() & UINT64_C(0xFFFFFFFFFFFF);
	int64_t  r = (int64_t)low + (int64_t)u % (int64_t)high;
	return (int)r;
}


double Rand_Float(void)
{
	uint64_t u = xorshift64s();
	int64_t  r = (int64_t)u  & INT64_C(0xFFFFFFFFFFFF);
	return (double)r / (double)INT64_C(0xFFFFFFFFFFFF);
}

//----------------------------------------------------------------------

int MatchNoCase(const char *A, const char *B)
{
	for (;;)
	{
		int AC = toupper(*A++);
		int BC = toupper(*B++);

		if (AC != BC)
			return 0;  // no match

		if (AC == 0)
			return 1;  // a match!
	}
}


int PartialMatch(const char *s, const char *prefix)
{
	while (*prefix != 0)
	{
		if (*s != *prefix)
			return 0;  // no match

		s += 1;
		prefix += 1;
	}

	return 1;  // a match!
}


int PartialMatchNoCase(const char *s, const char *prefix)
{
	while (*prefix != 0)
	{
		if (toupper(*s) != toupper(*prefix))
			return 0;  // no match

		s += 1;
		prefix += 1;
	}

	return 1;  // a match!
}

//----------------------------------------------------------------------

double CalcAngle(double dx, double dy)
{
	// result is in degrees, where east is 0, north is 90, etc...

	if (dx == 0)
		return (dy > 0) ? 90.0 : 270.0;

	double angle = atan2(dy, dx) * 180.0 / M_PI;

	if (angle < 0)
		angle += 360.0;

	return angle;
}


double AngleDiff(double a, double b)
{
	double diff = b - a;

	if (diff >=  180.0) diff -= 360.0;
	if (diff <= -180.0) diff += 360.0;

	return diff;
}


double PerpDist(double px, double py, double x1, double y1, double x2, double y2)
{
	// compute distance from point (px, py) to the line from (x1, y1)
	// to (x2, y2).  in other words, length of the perpendicular.

	px -= x1;
	py -= y1;

	x2 -= x1;
	y2 -= y1;

	double len = sqrt(x2*x2 + y2*y2);
	assert(len > 0.0);

	return (px * y2 - py * x2) / len;
}
